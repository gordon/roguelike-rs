use rltk::{BaseMap, Algorithm2D, Point};
use std::collections::HashSet;
use specs::prelude::*;
use serde::{Serialize, Deserialize};
pub use tiletype::{TileType, tile_opaque, tile_walkable, tile_cost};
pub use themes::*;

mod tiletype;
mod themes;

#[derive(PartialEq, Copy, Clone, Serialize, Deserialize, Debug)]
pub struct Rect {
    pub x1: i32,
    pub x2: i32,
    pub y1: i32,
    pub y2: i32
}

impl Rect {
    pub fn new(x: i32, y: i32, w: i32, h: i32) -> Rect {
        Rect {x1: x, x2: x + w, y1: y, y2: y + h}
    }

    // Returns true if this overlaps with another Rect
    pub fn intersect(&self, other: &Rect) -> bool {
        self.x1 <= other.x2 && self.x2 >= other.x1 && self.y1 <= other.y2 && self.y2 >= other.y1
    }

    pub fn center(&self) -> (i32, i32) {
        ((self.x1 + self.x2) / 2, (self.y1 + self.y2) / 2)
    }
}

#[derive(Default, Serialize, Deserialize, Clone)]
pub struct Map {
    pub tiles: Vec<TileType>,
    pub width: i32,
    pub height: i32,
    pub revealed_tiles: Vec<bool>,
    pub visible_tiles: Vec<bool>,
    pub blocked: Vec<bool>,
    pub depth: i32,
    pub bloodstains: HashSet<usize>,
    pub view_blocked: HashSet<usize>,
    pub name: String,

    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    pub tile_content: Vec<Vec<Entity>>
}

impl Map {
    pub fn xy_idx(&self, x: i32, y: i32) -> usize {
        (y as usize * self.width as usize) + x as usize
    }

    fn is_exit_valid(&self, x: i32, y: i32) -> bool {
        if x < 1 || x > self.width - 1 || y < 1 || y > self.height - 1 {
            return false;
        }
        let idx = self.xy_idx(x, y);
        !self.blocked[idx]
    }

    pub fn populate_blocked(&mut self) {
        for (i, tile) in self.tiles.iter_mut().enumerate() {
            self.blocked[i] = !tile_walkable(*tile);
        }
    }

    pub fn clear_content_index(&mut self) {
        for content in self.tile_content.iter_mut() {
            content.clear();
        }
    }

    /// Generates an empty map, consisting entirely of solid walls
    pub fn new<S: ToString>(new_depth: i32, width: i32, height: i32, name: S) -> Map {
        let map_tilecount = (width * height) as usize;
        Map {
            tiles: vec![TileType::Wall; map_tilecount],
            width,
            height,
            revealed_tiles: vec![false; map_tilecount],
            visible_tiles: vec![false; map_tilecount],
            blocked: vec!(false; map_tilecount),
            tile_content: vec![Vec::new(); map_tilecount],
            depth: new_depth,
            bloodstains: HashSet::new(),
            view_blocked: HashSet::new(),
            name: name.to_string(),
        }
    }

}

impl Algorithm2D for Map {
    fn dimensions(&self) -> Point {
        Point::new(self.width, self.height)
    }
}

impl BaseMap for Map {
    fn is_opaque(&self, idx: usize) -> bool {
        let idx_u = idx as usize;
        if idx_u > 0 && idx_u < self.tiles.len() {
            tile_opaque(self.tiles[idx_u]) || self.view_blocked.contains(&idx_u)
        } else {
            true
        }
    }

    fn get_pathing_distance(&self, idx1: usize, idx2: usize) -> f32 {
        let w = self.width as usize;
        let p1 = Point::new(idx1 % w, idx1 / w);
        let p2 = Point::new(idx2 % w, idx2 / w);
        rltk::DistanceAlg::Pythagoras.distance2d(p1, p2)
    }

    fn get_available_exits(&self, idx:usize) -> rltk::SmallVec<[(usize, f32); 10]> {
        // println!("get_available_exits on {}", idx);
        let mut exits = rltk::SmallVec::new();
        let x = idx as i32 % self.width;
        let y = idx as i32 / self.width;
        let w = self.width as usize;
        let tt = self.tiles[idx as usize];

        // Cardinal directions
        if self.is_exit_valid(x - 1, y) { exits.push((idx - 1, tile_cost(tt))) };
        if self.is_exit_valid(x + 1, y) { exits.push((idx + 1, tile_cost(tt))) };
        if self.is_exit_valid(x, y - 1) { exits.push((idx - w, tile_cost(tt))) };
        if self.is_exit_valid(x, y + 1) { exits.push((idx + w, tile_cost(tt))) };

        // Diagonals
        if self.is_exit_valid(x - 1, y - 1) { exits.push( ((idx - w) - 1, tile_cost(tt) * 1.45 )); }
        if self.is_exit_valid(x + 1, y - 1) { exits.push( ((idx - w) + 1, tile_cost(tt) * 1.45 )); }
        if self.is_exit_valid(x - 1, y + 1) { exits.push( ((idx + w) - 1, tile_cost(tt) * 1.45 )); }
        if self.is_exit_valid(x + 1, y + 1) { exits.push( ((idx + w) + 1, tile_cost(tt) * 1.45 )); }

        // println!("{}", exits.len());
        // let e_vec: Vec<(usize, f32)> = exits.clone().into_vec();
        // for e in e_vec {
        //     let e_idx: usize = e.0;
        //     println!("  - {}", e_idx);
        // }

        exits
    }
}
