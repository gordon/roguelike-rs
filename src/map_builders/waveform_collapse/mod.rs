use super::{MetaMapBuilder, BuilderMap, Map, TileType};
use rltk::RandomNumberGenerator;
use constraints::*;
use solver::*;
use common::*;

// mod image_loader;
mod constraints;
mod common;
mod solver;

#[derive(Debug)]
/// Provides a map builder using the Wave Function Collapse algorithm
pub struct WaveformCollapseBuilder {
    new_depth: i32,
}

impl MetaMapBuilder for WaveformCollapseBuilder {
    #[allow(dead_code)]
    fn build_map(&mut self, rng: &mut RandomNumberGenerator, build_data: &mut BuilderMap) {
        self.build(rng, build_data);
    }
}

impl WaveformCollapseBuilder {
    /// Generic constructor for waveform collapse.
    pub fn new() -> Box<WaveformCollapseBuilder> {
        Box::new(WaveformCollapseBuilder { new_depth: 0 })
    }

    fn build(&mut self, rng: &mut RandomNumberGenerator, build_data: &mut BuilderMap) {
        const CHUNK_SIZE: i32 = 8;
        self.new_depth = build_data.map.depth;
        build_data.take_snapshot();

        // Remove any stairs
        for t in build_data.map.tiles.iter_mut() {
            if *t == TileType::DownStairs { *t = TileType::Floor; }
        }

        let patterns = build_patterns(&build_data.map, CHUNK_SIZE, true, true);
        let constraints = patterns_to_constraints(patterns, CHUNK_SIZE);
        self.render_tile_gallery(&constraints, CHUNK_SIZE, build_data);

        build_data.map = Map::new(build_data.map.depth, build_data.width, build_data.height, &build_data.map.name);
        loop {
            let mut solver = Solver::new(constraints.clone(), CHUNK_SIZE, &build_data.map);
            while !solver.iteration(&mut build_data.map, rng) {
                build_data.take_snapshot();
            }
            build_data.take_snapshot();
            if solver.possible { break; } // If it has hit an impossible condition, try again
        }

        build_data.map.depth = self.new_depth;

        build_data.take_snapshot();

    }

    fn render_tile_gallery(&mut self, constraints: &Vec<MapChunk>, chunk_size: i32, build_data: &mut BuilderMap) {
        build_data.map = Map::new(0, build_data.width, build_data.height, &build_data.map.name);
        let mut counter = 0;
        let mut x = 1;
        let mut y = 1;
        while counter < constraints.len() {
            render_pattern_to_map(&mut build_data.map, &constraints[counter], chunk_size, x, y);

            x += chunk_size + 1;
            if x + chunk_size > build_data.map.width {
                // Move to the next row
                x = 1;
                x += chunk_size + 1;

                if y + chunk_size > build_data.map.height {
                    // Move to the next page
                    build_data.take_snapshot();
                    build_data.take_snapshot();
                    build_data.take_snapshot();
                    build_data.map = Map::new(0, build_data.width, build_data.height, &build_data.map.name);

                    x = 1;
                    y = 1;
                }
            }

            counter += 1;
        }
        build_data.take_snapshot();
        build_data.take_snapshot();
        build_data.take_snapshot();
    }
}
