use super::{MetaMapBuilder, BuilderMap, spawner};
use rltk::RandomNumberGenerator;

#[derive(Debug)]
pub struct RoomBasedSpawner {}

impl MetaMapBuilder for RoomBasedSpawner {
    fn build_map(&mut self, rng: &mut rltk::RandomNumberGenerator, build_data: &mut BuilderMap) {
        self.build(rng, build_data);
    }
}

impl RoomBasedSpawner {
    #[allow(dead_code)]
    pub fn new() -> Box<RoomBasedSpawner> {
        Box::new(RoomBasedSpawner {})
    }

    fn build(&mut self, rng: &mut RandomNumberGenerator, build_data: &mut BuilderMap) {
        if let Some(rooms) = &build_data.rooms {
            for room in rooms.iter().skip(1) {
                let mut room_inner = room.clone();
                room_inner.x1 += 1;
                room_inner.y1 += 1;
                room_inner.x2 -= 1;
                room_inner.y2 -= 1;
                spawner::spawn_room(&build_data.map, rng, &room_inner, build_data.map.depth, &mut build_data.spawn_list);
            }
        } else {
            panic!("Room based spawning only works after rooms have been created");
        }
    }
}
