use specs::prelude::*;
use super::{InflictsDamage, EntryTrigger, Hidden, Map, Position, EntityMoved, Name, gamelog::GameLog, SufferDamage, particle_system::ParticleBuilder, SingleActivation};
// use super::{Viewshed, Monster, Name, Map, Position, WantsToMelee};

pub struct EntryTriggerSystem {}

impl<'a> System<'a> for EntryTriggerSystem {
    type SystemData = (ReadExpect<'a, Map>,
                       ReadExpect<'a, Entity>,
                       Entities<'a>,
                       ReadStorage<'a, Position>,
                       ReadStorage<'a, EntryTrigger>,
                       ReadStorage<'a, InflictsDamage>,
                       WriteStorage<'a, EntityMoved>,
                       ReadStorage<'a, Name>,
                       WriteExpect<'a, GameLog>,
                       WriteStorage<'a, SufferDamage>,
                       WriteStorage<'a, Hidden>,
                       WriteExpect<'a, ParticleBuilder>,
                       ReadStorage<'a, SingleActivation>,
                      );

    fn run(&mut self, data: Self::SystemData) {
        let (map, player_entity, entities, position, entry_trigger, inflict_damage, mut entity_moved, names, mut gamelog, mut suffer_damage, mut hidden, mut particle_builder, single_activation) = data;

        let mut removed_entities: Vec<Entity> = Vec::new();
        for (entity, pos, _) in (&entities, &position, &entry_trigger).join() {
            // Is there an entity on this tile?
            let damages = inflict_damage.get(entity);
            let idx = map.xy_idx(pos.x, pos.y);
            let mut has_been_spotted = false;
            for e in map.tile_content[idx].iter() {
                let has_moved = entity_moved.get(*e);
                if let Some(_) = has_moved {
                    has_been_spotted = true;
                    let name = names.get(entity);
                    if *e == *player_entity {
                        if let Some(name) = name {
                            gamelog.entries.push(format!("You walk on {}!", name.name));
                        }
                    }

                    if let Some(damages) = damages {
                        // if entity == *player_entity {
                        //     if let Some(name) = name {
                        //         gamelog.entries.push(format!("You walk on {}!", name.name));
                        //     }
                        // }
                        SufferDamage::new_damage(&mut suffer_damage, *e, damages.damage);
                        particle_builder.request(pos.x, pos.y, rltk::RGB::named(rltk::ORANGE), rltk::RGB::named(rltk::BLACK), rltk::to_cp437('‼'), 200.0);
                    }

                }
            }
            if has_been_spotted {
                let is_hidden = hidden.get(entity);
                if let Some(_) = is_hidden {
                    hidden.remove(entity).expect("Unable to remove hidden");
                }
                let is_single_activation = single_activation.get(entity);
                if let Some(_) = is_single_activation {
                    removed_entities.push(entity);
                }
            }
        }

        for entity in removed_entities.iter() {
            entities.delete(*entity).expect("Unable to delete trap");
        }

        // Remove all entity movement markers
        entity_moved.clear();
    }
}

